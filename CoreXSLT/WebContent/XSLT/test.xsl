<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	    <xsl:copy>  
		<xsl:apply-templates select="node() | @*"/>		
		</xsl:copy>
		<xsl:comment>
		  <xsl:text>
		   Aadhya
		   </xsl:text>
		</xsl:comment>
	</xsl:template>
	
	<xsl:template match="*">
	  <xsl:copy>
	    <xsl:apply-templates select="node() | @*"/>
	  </xsl:copy>
	</xsl:template>
</xsl:stylesheet>