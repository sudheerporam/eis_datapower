<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jsonx="http://www.ibm.com/xmlns/prod/2009/jsonx" xmlns="http://www.datapower.com/schemas/transactions" xmlns:dp="http://www.datapower.com/extensions" extension-element-prefixes="dp" exclude-result-prefixes="dp">
       <xsl:output method="text" omit-xml-declaration="yes" />
       <xsl:template match="/">
          <dp:set-variable name="'var://service/mpgw/skip-backside'" value="1"/>
              <xsl:variable name="customData">
                     <xsl:apply-templates select="@* | node()" />
              </xsl:variable>
              <xsl:variable name="messageSize" select="format-number(string-length($customData) div 1024,'##0.00','format')"/>  
              <xsl:variable name="custom-category" select="'apimanalytics'"/>
              <xsl:message dp:type="{$custom-category}" dp:priority="info">  
                <xsl:value-of select="concat('Size=', $messageSize,'KB','|',$customData)" />
              </xsl:message>           
              <xsl:text>Hurray!!!</xsl:text>
       </xsl:template>
       <xsl:template match="@* | node()">
              <xsl:apply-templates select="@* | node()" />
       </xsl:template>
       
 
       <xsl:template match="*[local-name()='latencyInfo']">
              <xsl:for-each select="./jsonx:object">
                     <xsl:value-of select="concat(concat('L-',jsonx:string/text()),'=',jsonx:number/text(),'|')" />
              </xsl:for-each>
       </xsl:template>
 
       <xsl:template match="*[local-name()='query-string']">
              <xsl:for-each select="./*[local-name()='querystring']">
                     <xsl:value-of select="concat(concat('Q-',*[local-name()='arg']/@name ),'=',*[local-name()='arg'],'|')" />
              </xsl:for-each>
       </xsl:template>
       
       <xsl:template match="*[local-name()='headerarray-request']">
              <xsl:for-each select="./*[local-name()='header-array']/*[local-name()='header-element']">
                  <xsl:if test="*[local-name()='header-name']='Host' or *[local-name()='header-name']='X-Client-IP'">
                      <xsl:value-of select="concat(concat('HREQ-',*[local-name()='header-name'] ),'=',*[local-name()='header'],'|')" />                      
                  </xsl:if>
                                    </xsl:for-each>
       </xsl:template>
       <!-- 
        <xsl:template match="*[local-name()='headerarray-response']">
              <xsl:for-each select="./*[local-name()='header-array']/*[local-name()='header-element']">
                     <xsl:value-of select="concat(concat('HRES-',*[local-name()='header-name'] ),'=',*[local-name()='header'],'|')" />
              </xsl:for-each>
       </xsl:template> -->
       
       <xsl:template match="*[local-name()='datetime']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='source']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='apiId']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='apiName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='apiVersion']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='environmentId']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='environmentName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
 
       <xsl:template match="*[local-name()='planId']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='planName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='planVersion']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='organizationId']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='organization']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='orgName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='appId']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='appName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='apiUser']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='apiUserName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='statusCode']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='remoteHost']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='userAgent']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='requestProtocol']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='resourceId']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='resourceName']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='uriPath']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
       <xsl:template match="*[local-name()='requestMethod']">
              <xsl:value-of select="concat(name(),'=',.,'|')" />
       </xsl:template>
      
</xsl:stylesheet>